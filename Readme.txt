This module creates a Google like My Account block named 'My Account JQuery Block' which is pre-populated with details of logged-in user i.e. user name, account picture, login and logout links in drop down block with optional custom HTML. (see the snapshot)

The following can be configured:-
1. Choose username field bundle. You can select the bundle through which the user name will be displayed. A raw username is the login name of the user (e.g. james1) whereas User account field can be a full name (e.g. William James). User account fields can be configured in "admin/config/people/accounts/fields"
2. Last login date and time: Choose whether to display users last login date & time in My Account JQuery Block
3. User profile and Logout paths: The relative path of the logged-in user profile page and logout page.
4. Custom HTML can be added which can be shown in the drop down block. This can contain custom links to various offers, reports or sub-sites on which users have access to.
5. Block alignment: Block can be aligned to either left or right depending upon the region selected.

Configuration:
Install and enable this module like any other contributed module.
Go to admin/structure/blocks and look for My Account JQuery Block
Click on Config link, this will open the block configuration form
Save the desired values and select the region in which the block will be displayed
Come to home page.. the block will show the user picture in small scale (30x30) clicking on which a drop down block will appear showing more details.

The same functionality can be achieved with the help of Views and Pop-up module.

Note: If user has not uploaded any picture or default account picture is not provided then this module itself provides default pictures which can be changed in images folder under the module folder.
This module also creates two default image styles 'user_account_image_small' with 30x30 and 'user_account_image_large' with 100x100 scales
